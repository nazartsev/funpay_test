# README #

Парсер СМС от Яндекса.

### Тредования ###
- PHP 7.0 и выше
- Проверялось на PHP 7.1

### Тесты ###
- Пример тестов в файле test.php

### Как пользоваться ###

#### Подключение класса #### 
- `$parser = new ParserYandexSMS();`
- `$parser->parse($sms);`

#### Проверка правильности парсера ####
- `$parser->isParseCorrect()`

#### Получение данных ####
- Код подтверждения: `$parser->getPassword()`
- Номер кошелька: `$parser->getNumber()`
- Сумма для оплаты: `$parser->getPrice()`
- Получить все данные в виде массива: `$parser->getData()`

#### Проверка по полям ####
- Код подтверждения: `$parser->isFindPassword()`
- Номер кошелька: `$parser->isFindNumber()`
- Сумма для оплаты: `$parser->isFindPrice()`
- Получить все ошибки: `$parser->getErrors()`