<?php

class ParserYandexSMS
{
    private $price;
    private $number;
    private $password;

    public function parse(string $response): bool
    {
        $lines = explode("\n", $response);
        foreach ($lines as $line) {
            if (!$this->isFindNumber()) {
                $this->findNumber($line);
            }
            if (!$this->isFindPrice()) {
                $this->findPrice($line);
            }
            if (!$this->isFindPassword()) {
                $this->findPassword($line);
            }
        }

        return $this->isParseCorrect();
    }

    public function getErrors(): array
    {
        $errors = [];
        if (!$this->isFindPassword()) {
            $errors['password'] = 'Не найден код подтверждения';
        }
        if (!$this->isFindPrice()) {
            $errors['price'] = 'Не найдена сумма для списания';
        }
        if (!$this->isFindNumber()) {
            $errors['number'] = 'Не найден номер кошелька';
        }
        return $errors;
    }

    public function getData(): array
    {
        return [
            'password' => $this->getPassword(),
            'price' => $this->getPrice(),
            'number' => $this->getNumber(),
        ];
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getPrice(): string
    {
        return $this->price;
    }

    public function isFindPrice(): bool
    {
        return !empty($this->price);
    }

    public function isFindNumber(): bool
    {
        return !empty($this->number);
    }

    public function isFindPassword(): bool
    {
        return !empty($this->password);
    }

    public function isParseCorrect(): bool
    {
        return $this->isFindPassword() && $this->isFindPrice() && $this->isFindNumber();
    }

    private function findPassword(string $string): bool
    {
        if (preg_match('/([\d]+)/', $string, $password)) {
            $this->password = array_pop($password);
            return true;
        }
        return false;
    }

    private function findPrice(string $string): bool
    {
        if (preg_match('/([\d]+[\,\.]?[\d]{0,2})р/', $string, $price)) {
            $this->price = array_pop($price);
            return true;
        }
        return false;
    }

    private function findNumber(string $string): bool
    {
        if (preg_match('/(41001[\d]{6,11})/', $string, $number)) {
            $this->number = array_pop($number);
            return true;
        }

        return false;
    }
}